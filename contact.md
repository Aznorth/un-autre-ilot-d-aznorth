---
title: "Contact"
order: 1
in_menu: true
---
Je suis contactable à pleins d’endroits !

Mais voilà ma présence principale sur l’internet (derrière un pseudonyme 🥷 💀) :

[Aznörth, chez Framapiaf.org (Mastodon)](https://framapiaf.org/@Aznorth) 